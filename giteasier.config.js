function cleanupZombieBranches(ge) {
  const mainBranches = [ge.branch('master'), ge.branch('dev')];
  // get all the branch in the current repo
  ge.getBranches()
    // exclude the branches name 'master' and 'dev'
    .excludes(mainBranches)
    // includes only branches that their head are reachable by the current branch
    .excludes(branch => !branch.isMerged)
    // exclude the current branch
    .excludesCurrentBranch()
    // remove all the branches after all the exclusion.
    .deleteBranches();
}

function findBug(ge, args) {
  const fromPrev = args[0];
  const [bugCommit] = ge
    // get all the commits of the current branch
    .currentBranchCommits()
    // bisect on the current branch to find a commit the fail yarn.test()
    .autoBisect(fromPrev, commit => commit.checkout().yarn.test())
    // return the result as an array (length=1) of the fail commit
    .toList();

  // checkout at the commit that fails the yarn.test()
  bugCommit.checkout();

  // also return the information about that commit.
  return bugCommit;
}

function linearBisect(ge, args) {
  const isGoodCommit = args[0];
  const commits = ge
    .currentBranchCommits() // get all the commits of the current branch
    .reverse()              // reverse the order to be newer first
    .toList();              // return the list of branch.

  let oldestBadCommit;
  for (const commit of commits) {
    if (isGoodCommit(commit)) {
      // stop iteration when found a commit that is 'good' as defined by the input callback
      break;
    }
    oldestBadCommit = commit
  }

  oldestBadCommit.checkout(); // checkout the oldest 'bad' commit.
  return oldestBadCommit;
}