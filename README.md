# Final Project for UW CSE 402 Spring 2019

## Team member

- Chanwut Kittivorawong
- Manesh Jhawar
- Swojit Mohapatra

## Preproposal

- [Google Doc](https://docs.google.com/document/d/1DDtFupnK3sR453FrTnVk_E5neHuQrO1mLu_4MM5w0aI/edit?usp=sharing)

## Proposal

- [Google Doc](https://docs.google.com/document/d/1lscxbanzKuWGWc3fCRZjdazBp_FHHdO80sR2mHmr2OY/edit?usp=sharing)

## Design

- [Google Doc](https://docs.google.com/document/d/1DOt5A-TOU5Rie8YBKUDYnL7x15g_Wt-qRYZUVM7Aub8/edit?usp=sharing)

## Final Report

- [Google Doc](https://docs.google.com/document/d/1sqJrYGid1SW2LFiZjyNNoNJqc6bbCYIsoj4F_ASlrGk/edit?usp=sharing)

## Link to screencast

- [Youtube Video](https://www.youtube.com/watch?v=HGGrWqRDmuk&feature=youtu.be)

## Poster
- [Google Doc](https://drive.google.com/file/d/1CYOL03YyNfhdBmRqivaL-YjpV-0bbDGf/view?usp=sharing)

## Tutorial

GitEasier is a tool that helps users iterates through git information such as commits or branches and operates on them.

To start branch iteration, user can use `GitEasier.getBranches()`.
Then, users can add operations such as .excludeCurrentBranch() to remove the current branch from the iteration.
The code would look like this: `GitEasier.getBranches().excludeCurrentBranch()`
User can exclude some other branch as well by using `.exclude()`,
and they can just chain them together: `GitEasier.getBranches().excludeCurrentBranch().exclude('master')`.
We also provide an operation to `.excludeMerged()` and `.onlyMerged()` to be an another way to filter branches out.
In the end, we have to end the chain with ending operations. This operation will execute every operation in the chain
and then perform its own operation. For example, `.deleteBranches()` will delete all the branches created by the
chain. If we combine `GitEasier.getBranches().excludeCurrentBranch().exclude('master').onlyMerged().deleteBranches()`,
the chain will execute at the end that it would delete all the merged branches except master and current branch.

To start commits iteration, user can use `GitEasier.getCommits`.
Similarly to the branch iteration, users can chain operations.
For example, user can chain `.reverse()` to reverse the order of the commits.
Then, they can end the command with `.autoBisect()`, which execute; then, get the
result from the chain to perform bisection, base on the `autoBisect`'s callback.

Users can also extends the funcionality of GitEasier by adding custom functions into `giteasier.config.js`.
funcsions in `giteasier.config.js` will receive 2 parameters. First is ge, which is the `GitEasier` object.
Second is args, which is an array of additional parameters.
User can then use the custom function from the exported object `customFunction`.

## Worked Examples

### To Test findBug

checkout branch `test-autobisect`. Then, we provided the use of `customFunctions.findBug([7]);`
in the `index.js`, which calls the function `fundBug()` in the user config `giteasier.config.js`.
to run the program, type command `yarn start`
The program will then perform a bisection to find the first commit that contains a bug. And, set
HEAD to that commit.

### To Test linearBisect

This operation would result the same as autoBisect. You can find this example by checking out
branch `test-linearbisect` and run `yarn start`. We provided the use of `customFunctions.linearBisect([commit => commit.checkout().yarn.test()])` in the `index.js`,
which calls the function `linearBisect()` in the user config `giteasier.config.js`. This test shows that users can create their own
custom function to imitate the functionality of our provided autoBisect() but with linear search
instead.

### to test cleanupZombieBranches

checkout branch `test-cleanup-zombie`. We provided the use of `customFunctions.cleanupZombieBranches();` in the `index.js`,
which calls the function `cleanupZombieBranches()` in the user config `giteasier.config.js`..
Then, you can try creating branch in the existing commits under the `test-cleanup-zombie` branch.
After run `yarn start`, all the branches that their head is reachable from `test-cleanup-zombie` are removed.
