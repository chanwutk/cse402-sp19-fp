const execSync = require('child_process').execSync;

class Commit {
  _id;
  _author;
  _date;
  _message;

  constructor(id, author, date, message) {
    this._id = id;
    this._author = author;
    this._date = date;
    this._message = message;
  }

  get id() {
    return this._id;
  }

  get author() {
    return this._author;
  }

  get date() {
    return this._date;
  }

  get message() {
    return this._message;
  }

  get yarn() {
    return new YarnCommit(this._id, this._author, this._date, this._message);
  }

  get npm() {
    return new NpmCommit(this._id, this._author, this._date, this._message);
  }

  checkout() {
    try {
      if (!this.isCurrent()) execSync(`git checkout ${this._id}`);
    } catch (err) {
      throw new Error(`fail to checkout at ${this._id}`);
    }
    return this;
  }

  test() {
    throw new Error('Please specify package manager by calling .yarn.test() or .npm.test()');
  }

  isCurrent() {
    try {
      return execSync('git rev-parse --abbrev-ref HEAD').toString().trim() === this._id;
    } catch (err) {
      throw new Error('fail to get current branch name');
    }
  }

  equals(other) {
    return this.id === other.id;
  }
}

class YarnCommit extends Commit {
  constructor(id, author, date, message) {
    super(id, author, date, message);
  }

  test() {
    return test('yarn');
  }
}

class NpmCommit extends Commit {
  constructor(id, author, date, message) {
    super(id, author, date, message);
  }

  test() {
    return test('npm');
  }
}

function test(packageManager) {
  try {
    execSync(`${packageManager} test`);
    return true;
  } catch (err) {
    return false;
  }
}

module.exports = {
  Commit
};