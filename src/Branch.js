class Branch {
  _name;
  _isMerged;

  constructor(name, isMerged) {
    this._name = name;
    this._isMerged = isMerged;
  }

  get name() {
    return this._name;
  }

  get isMerged() {
    return this._isMerged;
  }

  equals(other) {
    return this.name === other.name;
  }
}

module.exports = {
  Branch
};