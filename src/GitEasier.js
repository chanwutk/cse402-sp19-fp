const { exec } = require('child_process');
const execSync = require('child_process').execSync;
const path = require('path');
const fs = require('fs');
const Commit = require('./Commit').Commit;
const Branch = require('./Branch').Branch;

class GitEasier {
  constructor(type) {
    this.type = type;
  }

  execute(_data) {
    throw new Error("Execute not implemented for " + this.type + " node.");
  }

  run(data) {
    return this.execute(data);
  }

  reverse() {
    return new GitEasierThen(this, new GitEasierReverse());
  }

  excludes(toExcludes) {
    return new GitEasierThen(this, new GitEasierExcludes(toExcludes));
  }

  excludesCurrentBranch() {
    if (this.iteratingOn !== 'branch') {
      throw new Error('excludesCurrentBranch cannot operate on non-branch iterator');
    }

    const branch = GitEasier.currentBranch();
    return this.excludes(branch)
  }

  autoBisect(fromPrev, isGoodCommit) {
    return new GitEasierThen(this, new GitEasierAutoBisect(fromPrev, isGoodCommit));
  }

  // end of chain
  deleteBranches() {
    if (this.iteratingOn !== 'branch') {
      throw new Error('deleteBranches cannot operate on non-branch iterator');
    }
    this.execute().forEach(branch => {
      exec(`git branch -d ${branch.name}`, (err, stdout, stderr) => {
        if (err) {
          throw new Error('delete fails');
        }

        if (stderr) {
          console.error(`stdout: ${stdout}`);
        }

        console.log(`stdout: ${stdout}`);
      });
    });
  }

  toList() {
    return this.execute();
  }

  // statics
  static getBranches() {
    return new GitEasierBranches();
  }

  static getCommits(branch) {
    return new GitEasierCommits(branch);
  }

  static currentBranchCommits() {
    const branch = GitEasier.currentBranch();
    return new GitEasierCommits(branch);
  }

  static test(packageManager) {
    try {
      execSync(`${packageManager} test`);
      return true;
    } catch (err) {
      return false;
    }
  }

  static add(files) {
    exec(`git add ${Array.isArray(files) ? files.join(' ') : files}`, (err, stdout, stderr) => {
      if (err) {
        throw new Error('add fails');
      }

      if (stderr) {
        console.error(`stdout: ${stdout}`);
      }

      console.log(`stdout: ${stdout}`);
    });
  }

  static addAll() {
    exec('git add -A', (err, stdout, stderr) => {
      if (err) {
        throw new Error('addAll fails');
      }

      if (stderr) {
        console.error(`stdout: ${stdout}`);
      }

      console.log(`stdout: ${stdout}`);
    });
  }

  static commit(message = 'no message') {
    exec(`git commit -m ${message}`, (err, stdout, stderr) => {
      if (err) {
        throw new Error('commit fails');
      }

      if (stderr) {
        console.error(`stdout: ${stdout}`);
      }

      console.log(`stdout: ${stdout}`);
    });
  }

  static pull() {
    exec('git pull', (err, stdout, stderr) => {
      if (err) {
        throw new Error('pull error');
      }

      if (stderr) {
        console.error(`stderr: ${stderr}`);
      }

      console.log(`stdout: ${stdout}`);
    });
  }

  static push() {
    exec(`git push`, (err, stdout, stderr) => {
      if (err) {
        throw new Error('push error');
      }

      if (stderr) {
        console.error(`cannot push: ${stderr}`);
      } else {
        console.log(`stdout: ${stdout}`);
      }
    });
  }

  static checkoutCommit(commitId) {
    new Commit(commitId).checkout();
  }

  static branch(name) {
    return new Branch(name);
  }

  static currentBranch() {
    try {
      const name = execSync('git rev-parse --abbrev-ref HEAD').toString().trim();
      return new Branch(name);
    } catch (err) {
      throw new Error('fail to get current branch name');
    }
  }
}

class GitEasierThen extends GitEasier {
  constructor(first, second) {
    super("Then");
    this.first = first;
    this.second = second;
    this.iteratingOn = first.iteratingOn;
  }

  execute(data) {
    return this.second.execute(this.first.execute(data));
  }
}

class GitEasierBranches extends GitEasier {
  constructor() {
    super("Branch");
    this.iteratingOn = 'branch';
  }

  execute() {
    try {
      const all = parseBranches(execSync(`git branch | cat`).toString());
      const merged = parseBranches(execSync(`git branch --merged | cat`).toString());
      return all.map(branch => new Branch(branch, merged.includes(branch)));
    } catch (err) {
      throw new Error('fail to get list of branches');
    }
  }
}

function parseBranches(str) {
  return str.split('\n').slice(0, -1).map(branch => {
    const trimmed = branch.trim();
    if (branch.charAt(0) === '*') {
      return trimmed.substring(1).trim();
    } else {
      return trimmed;
    }
  });
}

class GitEasierCommits extends GitEasier {

  constructor(branch) {
    super("GetCommits");
    this.iteratingOn = 'commit';
    this.branch = branch;
  }

  execute() {
    const out = execSync(`git log --reverse ${this.branch.name.trim()} | cat`).toString();
    return parseCommits(out);
  }
}

function parseCommits(str) {
  return str.split('\ncommit').slice(1).map(commit => {
    const info = commit.trim().split('\n').map(i => i.trim());
    const id = info[0].split(' ')[0];
    const author = info[1].substring(8).trim();
    const date = new Date(info[2].substring(8).trim());
    const message = info[4].trim();
    return new Commit(id, author, date, message);
  });
}

class GitEasierExcludes extends GitEasier {
  constructor(toExcludes) {
    super('excludes');
    this.isCallback = typeof toExcludes === 'function';
    this.toExcludes = toExcludes ? Array.isArray(toExcludes) || this.isCallback ? toExcludes : [toExcludes] : [];
  }

  execute(data) {
    if (this.isCallback) {
      return data.filter(d => !this.toExcludes(d));
    } else {
      const out = [];
      for (const d of data) {
        let toExclude = false;
        for (const e of this.toExcludes) {
          if (d.equals(e)) {
            toExclude = true;
          }
        }
        if (!toExclude) {
          out.push(d);
        }
      }
      return out;
    }
  }
}

class GitEasierAutoBisect extends GitEasier {
  constructor(fromPrev, isGoodCommit) {
    super('auto-bisect');
    this.isGoodCommit = isGoodCommit;
    this.fromPrev = fromPrev;
  }

  execute(commits) {
    let l = commits.length - 1 - this.fromPrev, r = commits.length - 1;
    if (l < 0) l = 0;

    if (!this.isGoodCommit(commits[l])) {
      return [commits[l]];
    }

    if (this.isGoodCommit(commits[r])) {
      return [];
    }

    while (r - l > 1) {
      const mid = ~~((l + r) / 2);
      if (this.isGoodCommit(commits[mid])) {
        l = mid;
      } else {
        r = mid;
      }
    }

    commits[r].checkout();
    return [commits[r]]
  }
}

class GitEasierReverse extends GitEasier {
  execute(data) {
    return data.reverse();
  }
}

const customFunctions = {};
const config = fs.readFileSync(path.join(process.cwd(), 'giteasier.config.js')).toString();
config.split('function').slice(1).forEach(functionText => {
  const trimmed = functionText.trim();
  const functionName = trimmed.substring(0, trimmed.indexOf('(')).trim();
  const startIdx = trimmed.indexOf('{') + 1;
  const endIdx = trimmed.lastIndexOf('}');
  const func = new Function('ge', 'args', trimmed.substring(startIdx, endIdx));
  customFunctions[functionName] = (args) => func(GitEasier, args);
});

module.exports = {
  GitEasier,
  customFunctions
};
