const { GitEasier, customFunctions } = require("./src/GitEasier");

// test cleanup branch
customFunctions.cleanupZombieBranches();

// test findBug
console.log(customFunctions.findBug([7]));

// test linear bisect
console.log(customFunctions.linearBisect([commit => commit.checkout().yarn.test()]));

module.export = {GitEasier, customFunctions};